/*
    App.js server side
    Express middleware
*/
var express = require("express");
var app = express();
var bodyParser = require("body-parser");

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit:'50mb'}));

app.get("/api/Calculator",(req,res)=>{
    console.log("Get calculator");
    res.status(200).json(Calculator);
})

app.use((req,res)=>{
    var x = 6;
    try{
        res.send("<h1>Oops, wrong page</h1>");
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    } 
});

app.listen(NODE_PORT, ()=>{
    console.log(`Web App started at ${NODE_PORT}`);
    //console.log("Web App started at " + NODE_PORT);
});