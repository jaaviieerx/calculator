/* 
 Client side app.js
 AngularJS
 */

memory  = "0";
current = "0";
operation = 0;
maxLength = 30;

var self = this;


function addDigit(dig){
    if (current.length > maxLength){
      current = "Too long!";
      }else{
        if((eval(current) == 0)
          && (current.indexOf(".") == -1)){
          current = dig;
        }else{ 
          current = current + dig;
          };
   };};
  
   self.Calculator.Display.value = current;


  function dot()
  {
   if ( current.length == 0)
     { current = "0.";
     } else
     {  if ( current.indexOf(".") == -1)
          { current = current + ".";
     };   };
   self.Calculator.Display.value = current;
  }

  function operate(op)
  {
   if (op.indexOf("*") > -1) {operation = 1;};
   if (op.indexOf("/") > -1) {operation = 2;};
   if (op.indexOf("+") > -1) {operation = 3;};
   if (op.indexOf("-") > -1) {operation = 4;};
 
   memory = current;
   current = "";
   self.Calculator.Display.value = current;
  }