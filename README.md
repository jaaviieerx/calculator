### How do I set up? ###

* git init
* git remote add origin <git URL>
* npm init (answer all the questions, enter the entry point accordingly)
* mkdir <server directory>
* create an app.js under server directory
* create .gitignore to ignore directories/files
* npm install express --save

### How do I commit? ###

* git add .
* git commit -m "Message"
* git push origin master -u

### How to make a website? ###

* Create /server and /client folders
* Create /server/app.js
* Create /client/css and /client/app
* Create /css/main.css
* Create /server/app.js
* Create /client/cal.html
* Create .gitignore to ignore /node_modules & /client/bower_components
* Run "npm install body-parser"
* Run "npm install -g bower"
* Run "bower init" and "bower install bootstrap"

### How to make a calculator app? ###

* Create a basic /server/app.js for website launching
* Create basic /client/index.html for a simple template of website for calculator
* Create /client/calc.js for defining calculator functions
* Finish up and beautify the website on index.html
* Try out the calculator functions before launch